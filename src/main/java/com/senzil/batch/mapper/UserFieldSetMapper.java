package com.senzil.batch.mapper;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.apache.log4j.Logger;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import com.senzil.batch.model.User;
import com.senzil.batch.util.DateFormats;
import com.senzil.batch.util.DateUtils;
import com.senzil.batch.util.UserCols;


/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public class UserFieldSetMapper implements FieldSetMapper<User> {

	private static final Logger LOGGER = Logger.getLogger(UserFieldSetMapper.class);

	public User mapFieldSet(FieldSet fieldSet) throws BindException {
		User item = new User();
		item.setName(fieldSet.readString(UserCols.NAME.getName()));
		item.setLastName(fieldSet.readString(UserCols.LAST_NAME.getName()));

		String dateString = fieldSet.readString(UserCols.BIRTH_DATE.getName());
		item.setBirthDate(DateUtils.parseDate(dateString, DateFormats.dd_MM_yyyy));

		LOGGER.info("Mapped fields for user instance [" + item + "].");
		return item;

	}

}
