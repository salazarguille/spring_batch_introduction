package com.senzil.batch.processor;

import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemProcessor;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 * @param <T>
 * @param <H>
 */
public class CustomItemProcessor<T, H> implements ItemProcessor<T, H> {
	
	private static final Logger LOGGER = Logger.getLogger(CustomItemProcessor.class);
	
    @SuppressWarnings("unchecked")
	public H process(T item) {
        LOGGER.info("Processing item [" + item + "].");
        return (H) item;
    }
}