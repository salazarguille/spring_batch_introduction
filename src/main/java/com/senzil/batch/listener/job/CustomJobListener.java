package com.senzil.batch.listener.job;

import org.apache.log4j.Logger;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public class CustomJobListener implements JobExecutionListener {

	private static final Logger LOGGER = Logger.getLogger(CustomJobListener.class);

	@Override
	public void beforeJob(JobExecution jobExecution) {
		LOGGER.info("Before job.");
	}

	@Override
	public void afterJob(JobExecution jobExecution) {
		LOGGER.info("After job.");
	}
}
