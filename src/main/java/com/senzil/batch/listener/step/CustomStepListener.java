package com.senzil.batch.listener.step;

import org.apache.log4j.Logger;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public class CustomStepListener implements StepExecutionListener {

	private static final Logger LOGGER = Logger.getLogger(CustomStepListener.class);

	@Override
	public void beforeStep(StepExecution stepExecution) {
		LOGGER.info("Before step.");
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		LOGGER.info("After step.");
		return stepExecution.getExitStatus();
	}
}
