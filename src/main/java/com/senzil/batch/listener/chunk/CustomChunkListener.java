package com.senzil.batch.listener.chunk;

import org.apache.log4j.Logger;
import org.springframework.batch.core.ChunkListener;
import org.springframework.batch.core.scope.context.ChunkContext;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public class CustomChunkListener implements ChunkListener {

	private static final Logger LOGGER = Logger.getLogger(CustomChunkListener.class);

	@Override
	public void beforeChunk(ChunkContext context) {
		LOGGER.info("Before chunk.");
	}

	@Override
	public void afterChunk(ChunkContext context) {
		LOGGER.info("After chunk.");
	}

	@Override
	public void afterChunkError(ChunkContext context) {
		LOGGER.info("After chunk error.");
	}
}
