/**
 * 
 */
package com.senzil.batch.util;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public enum UserCols {

	NAME("Name"),
	LAST_NAME("LastName"),
	BIRTH_DATE("BirthDate")
	;
	
	private String name;
	
	private UserCols(String name) {
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
