package com.senzil.batch.util;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public final class JobParams {
	
	protected static final String PREFIX_STEP_PARAMS = "#{stepExecutionContext[";
	protected static final String PREFIX_JOB_PARAMS = "#{jobParameters[";
	protected static final String SUFFIX_PARAMS = "]}";
	
	public static final String OUTPUT_USER_XML_FILE = "outputUserXmlFile";
	public static final String OUTPUT_USER_XML_FILE_BINDING = PREFIX_JOB_PARAMS + OUTPUT_USER_XML_FILE + SUFFIX_PARAMS;
	
	public static final String INPUT_USER_CSV_FILE = "inputUserCsvFile";
	public static final String INPUT_USER_CSV_FILE_BINDING = PREFIX_JOB_PARAMS + INPUT_USER_CSV_FILE + SUFFIX_PARAMS;
	
	public static final String COMMIT_INTERVAL = "commitInterval";
	public static final String COMMIT_INTERVAL_BINDING = PREFIX_JOB_PARAMS + COMMIT_INTERVAL + SUFFIX_PARAMS;
	
	private JobParams() {
		super();
	}
	
}