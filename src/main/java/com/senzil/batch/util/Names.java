package com.senzil.batch.util;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public final class Names {
	
	public static final String USER_XML_ITEM_READER = "userXmlItemReader";
	public static final String USER_CSV_ITEM_READER = "userCsvItemReader";
	public static final String JOB_LAUNCHER = "jobLauncher";
	
	private Names() {
		super();
	}
	
}