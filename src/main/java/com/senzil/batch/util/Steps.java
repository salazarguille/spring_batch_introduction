package com.senzil.batch.util;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public final class Steps {
	
	public static final String READ_USER_CVS_FILE = "readUserCvsFileStep";
	
	public static final String READ_USER_XML_FILE = "readUserXmlFileStep";
	
	private Steps() {
		super();
	}
	
}