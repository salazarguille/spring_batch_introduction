package com.senzil.batch.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public final class DateUtils {
	private static final Logger LOGGER = Logger.getLogger(DateUtils.class);

	private DateUtils() {
		super();
	}

	public static Date parseDate(String dateString, String dateFormatString) {
        Date date = null;
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat(dateFormatString);
			date = dateFormat.parse(dateString);
		} catch (ParseException e) {
			LOGGER.error("Error parsing date.", e);
		}
        return date;
	}
}