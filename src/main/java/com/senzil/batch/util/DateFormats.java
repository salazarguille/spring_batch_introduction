package com.senzil.batch.util;


/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public final class DateFormats {
	
	public static final String dd_MM_yyyy = "dd/MM/yyyy";
	
	private DateFormats() {
		super();
	}
	
}