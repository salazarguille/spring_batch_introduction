package com.senzil.batch.config;


import java.text.ParseException;
import org.springframework.batch.core.ChunkListener;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.xml.StaxEventItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import com.senzil.batch.listener.chunk.CustomChunkListener;
import com.senzil.batch.listener.job.CustomJobListener;
import com.senzil.batch.listener.step.CustomStepListener;
import com.senzil.batch.mapper.UserFieldSetMapper;
import com.senzil.batch.model.User;
import com.senzil.batch.processor.CustomItemProcessor;
import com.senzil.batch.reader.CustomFlatFileItemReader;
import com.senzil.batch.util.Consts;
import com.senzil.batch.util.JobParams;
import com.senzil.batch.util.Jobs;
import com.senzil.batch.util.Names;
import com.senzil.batch.util.Steps;
import com.senzil.batch.util.UserCols;
import com.senzil.batch.writer.CustomStaxEventItemWriter;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public class SpringBatchConfig {
    @Autowired
    private JobBuilderFactory jobs;

    @Autowired
    private StepBuilderFactory steps;
    
    @Bean
    public JobExecutionListener customJobListener() {
    	return new CustomJobListener();
    }
    
    @Bean
    public ChunkListener customChunkListener() {
    	return new CustomChunkListener();
    }
    
    @Bean
    public StepExecutionListener customStepListener() {
    	return new CustomStepListener();
    }
    
    /*
    @Bean
    @StepScope
    public StaxEventItemReader<User> userXmlItemReader() {
    	final StaxEventItemReader<User> itemReader = new StaxEventItemReader<>();
    	itemReader.setUnmarshaller(this.userXmlUnmarshaller());
    	return itemReader;
    }

	private Jaxb2Marshaller userXmlUnmarshaller() {
		final Map<String, Boolean> properties = new HashMap<>();
		properties.put(UnmarshallerProperties.UNMARSHALLING_CASE_INSENSITIVE, Boolean.TRUE);
		final Jaxb2Marshaller unMarshaller = new Jaxb2Marshaller();
    	unMarshaller.setUnmarshallerProperties(properties);
		return unMarshaller;
	}*/

    @Bean
    @StepScope
    public FlatFileItemReader<User> userCsvItemReader(@Value(JobParams.INPUT_USER_CSV_FILE_BINDING) Resource inputUserResource) throws UnexpectedInputException, ParseException {
    	DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer();
    	String[] tokens = { UserCols.NAME.getName(), UserCols.LAST_NAME.getName(), UserCols.BIRTH_DATE.getName()};
    	tokenizer.setNames(tokens);

    	DefaultLineMapper<User> lineMapper = new DefaultLineMapper<User>();
    	lineMapper.setLineTokenizer(tokenizer);
    	lineMapper.setFieldSetMapper(new UserFieldSetMapper());

    	CustomFlatFileItemReader<User> reader = new CustomFlatFileItemReader<User>();
        reader.setResource(inputUserResource);
        reader.setLinesToSkip(1);
        reader.setLineMapper(lineMapper);
        return reader;
    }

    @Bean
    @StepScope
    public ItemProcessor<User, User> itemProcessor() {
        return new CustomItemProcessor<User, User>();
    }

    @Bean
    @StepScope
    public StaxEventItemWriter<User> usersDataXmlItemWriter(@Value(JobParams.OUTPUT_USER_XML_FILE_BINDING) Resource ouputUserResource) {
        CustomStaxEventItemWriter<User> itemWriter = new CustomStaxEventItemWriter<User>();
        itemWriter.setRootTagName(Consts.USERS_DATA);
        itemWriter.setVersion("1.0");
        itemWriter.setForceSync(true);
        itemWriter.setResource(ouputUserResource);
        itemWriter.setMarshaller(this.marshaller());
        return itemWriter;
    }

    @Bean
    @StepScope
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setClassesToBeBound(new Class[] { User.class });
        return marshaller;
    }

    /**
     * This step reads a CSV file, and creates a XML file.
     * 
     * @param commitInterval
     * @param reader
     * @param processor
     * @param writer
     * @return
     */
    @Bean(name = Steps.READ_USER_CVS_FILE)
    @JobScope
    protected Step readUserCvsFile(@Qualifier(Names.USER_CSV_ITEM_READER) ItemReader<User> reader, ItemProcessor<User, User> processor, ItemWriter<User> writer) {
        return steps.get(Steps.READ_USER_CVS_FILE)
        		.<User, User> chunk(10)
        		.reader(reader)
        		.processor(processor)
        		.writer(writer)
        		.listener(this.customChunkListener())
        		.listener(this.customStepListener())
        		.build();
    }
/*
    @Bean(name = Steps.READ_USER_XML_FILE)
    @JobScope
    protected Step readUserXmlFile(@Value(JobParams.COMMIT_INTERVAL_BINDING) Integer commitInterval, @Qualifier(Names.USER_XML_ITEM_READER) ItemReader<User> reader, ItemProcessor<User, User> processor, ItemWriter<User> writer) {
        return steps.get(Steps.READ_USER_XML_FILE)
        		.<User, User> chunk(commitInterval)
        		.reader(reader)
        		.processor(processor)
        		.writer(writer)
        		.build();
    }
*/
    /**
     * It creates a configured Spring Batch {@link Job} instance.
     * 
     * @param readUserCsvFileStep step which reads a CSV file and writes a XML file.
     * @return a {@link Job} instance.
     */
    @Bean(name = Jobs.PROCESS_USERS_FILE)
    public Job job(@Qualifier(Steps.READ_USER_CVS_FILE) Step readUserCsvFileStep) {
        return jobs.get(Jobs.PROCESS_USERS_FILE)
        		.start(readUserCsvFileStep)
        		.listener(this.customJobListener())
        		.build();
    }
}
