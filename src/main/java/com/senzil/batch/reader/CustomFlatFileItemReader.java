package com.senzil.batch.reader;

import org.apache.log4j.Logger;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.file.FlatFileItemReader;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 * @param <T>
 * @param <H>
 */
public class CustomFlatFileItemReader<T> extends FlatFileItemReader<T> {
	
	private static final Logger LOGGER = Logger.getLogger(CustomFlatFileItemReader.class);

	@Override
	public T read() throws UnexpectedInputException, ParseException, Exception {
		T item = super.read();
		LOGGER.info("Reading item [" + item + "].");
		return item;
	}
}