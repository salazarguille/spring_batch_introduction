package com.senzil.batch;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.senzil.batch.config.SpringBatchConfig;
import com.senzil.batch.config.SpringConfig;
import com.senzil.batch.util.JobParams;
import com.senzil.batch.util.Jobs;
import com.senzil.batch.util.Names;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 */
public class App {
	
	private static final Logger LOGGER = Logger.getLogger(App.class);
	private static AnnotationConfigApplicationContext context;
	
    public static void main(final String[] args) {
        context = new AnnotationConfigApplicationContext();
        context.register(SpringConfig.class);
        context.register(SpringBatchConfig.class);
        context.refresh();

        final JobLauncher jobLauncher = (JobLauncher) context.getBean(Names.JOB_LAUNCHER);
        final Job job = (Job) context.getBean(Jobs.PROCESS_USERS_FILE);
        LOGGER.info("Starting the batch job");
        try {
        	Map<String, JobParameter> parameters = new HashMap<>();
        	parameters.put(JobParams.COMMIT_INTERVAL, new JobParameter("10"));
        	parameters.put(JobParams.INPUT_USER_CSV_FILE, new JobParameter("csv/usersList.csv"));
        	parameters.put(JobParams.OUTPUT_USER_XML_FILE, new JobParameter("file:output/xml/usersList.xml"));
        	
            final JobExecution execution = jobLauncher.run(job, new JobParameters(parameters));
            LOGGER.info("Job Status : " + execution.getStatus());
            LOGGER.info("Job succeeded");
        } catch (final Exception e) {
        	String errorMessage = "Job failed: [" + e.getMessage() + "].";
            LOGGER.error(errorMessage, e);
            throw new RuntimeException(errorMessage, e);
        }
    }
}