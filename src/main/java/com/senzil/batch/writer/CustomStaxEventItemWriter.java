package com.senzil.batch.writer;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.batch.item.xml.StaxEventItemWriter;
import org.springframework.oxm.XmlMappingException;

/**
 * TODO Add comments.
 * 
 * @author <a href="mailto:guillermo@senzil.com">Guille Salazar</a>
 *
 * @param <T>
 * @param <H>
 */
public class CustomStaxEventItemWriter<T> extends StaxEventItemWriter<T> {
	
	private static final Logger LOGGER = Logger.getLogger(CustomStaxEventItemWriter.class);

	@Override
	public void write(List<? extends T> items) throws XmlMappingException, Exception {
		LOGGER.info("Writing [" + items.size() + "] items.");
		super.write(items);
	}
}