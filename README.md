# README #


This is a maven project based on Java 8 and Spring Batch Framework. It will allow you to run a Java main class using a Spring Batch configuration.

### What does it do? ###

This application reads a CSV file, process it, and creates a XML file.

### How do you run the application? ###

* Open class *com.senzil.batch.App*
* Right Click -> *Run as Main Application*.
* Enjoy it

### Comments and / or Questions ###

* You can send an email to *guillermo@senzil.com*